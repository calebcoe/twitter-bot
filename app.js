/*
This is a twitter bot. It will do 4 things
The bot will search through tweets usings parameters set in params
With the results the bot will then like(favorite) the tweet, retweet it, and follow the tweeter
*/


//Conficugation
var Twitter = require('twitter'); //Twitter package
var config = require('./config.js'); //Local file
var T = new Twitter(config); //NPM package that interacts with Twitter API

// Set up your search parameters
var params = {
  q: '#javascript',
  count: 5,
  result_type: 'recent',
  lang: 'en'
}

//Get will seach for tweets that fit our given params
T.get('search/tweets', params, function(err, data, response) {
	//If there is no error favorite, retweet and follow
	if(!err){
		//Loop through all the tweets (number of tweets is based on count)
		for(let i=0; i<data.statuses.length; i++) {
			//Get this specific tweet id
			let id = {
				id: data.statuses[i].id_str
			}
			//Get this specific tweet username
			let screen_name = data.statuses[i].user.screen_name;
			T.post('favorites/create', id, function(err, response){ 
				//If there is no error for the POST then favorite the post
				if(!err) {
					let username = response.user.screen_name;
					let tweetId = response.id_str;
					let s = `https://twitter.com/${username}/status/${tweetId}`;
					console.log('Favorited: ', s)
				} else {
					console.log(err)
				}
			});
			//Retweet the post
			T.post('statuses/retweet/', id, function(err, response){
			    if(!err){			    	
			    	console.log('Retweeted: ', screen_name);
			    } else {
			    	console.log(err);
			    }
			});
			//Follow the user who made the post
			T.post('friendships/create', {screen_name}, function(err, response){
			    if(!err){
			    	console.log(screen_name, ': **FOLLOWED**');
			    } else {
			    	console.log(err);
			    }
			});					
		}
	} else {
		console.log(err);
	}
})
